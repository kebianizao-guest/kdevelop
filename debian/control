Source: kdevelop
Section: devel
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Jeremy Lainé <jeremy.laine@m4x.org>,
           George Kiagiadakis <kiagiadakis.george@gmail.com>,
           Andreas Cord-Landwehr <cola@uni-paderborn.de>
Build-Depends: bash-completion,
               clang (>= 1:6.0) [!alpha !hppa !ia64 !kfreebsd-any !m68k !powerpcspe !riscv64 !sh4 !x32],
               cmake (>= 2.8.12),
               debhelper-compat (= 12),
               dh-exec,
               extra-cmake-modules (>= 5.59~),
               gettext,
               kdevelop-pg-qt (>= 2.2.0),
               libboost-serialization-dev,
               libastyle-dev (>= 3.1),
               libclang-dev (>= 1:6.0) [!alpha !hppa !ia64 !kfreebsd-any !m68k !powerpcspe !riscv64 !sh4 !x32],
               libgrantlee5-dev,
               libkf5archive-dev (>= 5.28.0),
               libkf5config-dev (>= 5.28.0),
               libkf5crash-dev (>= 5.28.0),
               libkf5declarative-dev (>= 5.28.0),
               libkf5doctools-dev (>= 5.28.0),
               libkf5guiaddons-dev (>= 5.28.0),
               libkf5i18n-dev (>= 5.28.0),
               libkf5iconthemes-dev (>= 5.28.0),
               libkf5itemmodels-dev (>= 5.28.0),
               libkf5itemviews-dev (>= 5.28.0),
               libkf5jobwidgets-dev (>= 5.28.0),
               libkf5kcmutils-dev (>= 5.28.0),
               libkf5kio-dev (>= 5.28.0),
               libkf5newstuff-dev (>= 5.28.0),
               libkf5notifications-dev (>= 5.28.0),
               libkf5notifyconfig-dev (>= 5.28.0),
               libkf5parts-dev (>= 5.28.0),
               libkf5plasma-dev [linux-any],
               libkf5purpose-dev (>= 5.44.0),
               libkf5runner-dev [linux-any],
               libkf5service-dev (>= 5.28.0),
               libkf5sonnet-dev (>= 5.28.0),
               libkf5sysguard-dev,
               libkf5texteditor-dev (>= 5.28.0),
               libkf5threadweaver-dev (>= 5.28.0),
               libkf5widgetsaddons-dev (>= 5.28.0),
               libkf5windowsystem-dev (>= 5.28.0),
               libkf5xmlgui-dev (>= 5.28.0),
               libkomparediff2-dev,
               libqt5webkit5-dev (>= 5.7.0),
               libsvn-dev,
               llvm-dev (>= 1:6.0) [!alpha !hppa !ia64 !kfreebsd-any !m68k !powerpcspe !riscv64 !sh4 !x32],
               okteta-dev (>= 5:0.26.2~),
               pkg-config,
               pkg-kde-tools (>= 0.15.16),
               qtbase5-dev (>= 5.7.0),
               qtdeclarative5-dev (>= 5.7.0),
               qttools5-dev,
               qttools5-dev-tools,
               shared-mime-info
Standards-Version: 4.5.0
Homepage: https://www.kdevelop.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/kdevelop
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/kdevelop.git

Package: kdevelop55-libs
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: shared libraries for the KDevelop platform
 This package contains shared libraries needed to run integrated development
 environments based on the KDevelop platform.
 .
 This package is part of the KDevelop platform.

Package: kdevelop
Architecture: any
Depends: kdevelop-data (>= ${source:Version}),
         kdevelop55-libs (= ${binary:Version}),
         kinit,
         qml-module-qtquick-controls,
         qml-module-qtquick-layouts,
         qml-module-qtquick-window2,
         qml-module-qtquick-xmllistmodel,
         qml-module-qtquick2,
         qml-module-qtwebkit,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: ${kdevelop:clang}, g++, gcc, gdb (>= 7.0), kapptemplate, kio-extras, make
Suggests: clang,
          clang-tidy,
          clazy,
          cmake,
          cppcheck,
          git,
          heaptrack [linux-any],
          kdevelop-l10n,
          konsole,
          meson,
          ninja-build
Breaks: kdevplatform10-libs (<< 5.2)
Replaces: kdevplatform10-libs (<< 5.2)
Description: integrated development environment for C/C++ and other languages
 KDevelop is a Free and Open Source integrated development
 environment (IDE). It provides editing, navigation and debugging features for
 several programming languages, as well as integration with multiple build
 systems and version-control systems, using a plugin-based architecture.
 .
 KDevelop has parser backends for C, C++ and Javascript/QML, with further
 external plugins supporting e.g. PHP or Python.

Package: kdevelop-data
Architecture: all
Depends: ${misc:Depends}
Breaks: kdevplatform10-libs (<< 5.2)
Replaces: kdevplatform10-libs (<< 5.2)
Description: data files for the KDevelop IDE
 KDevelop is a Free and Open Source integrated development
 environment (IDE). It provides editing, navigation and debugging features for
 several programming languages, as well as integration with multiple build
 systems and version-control systems, using a plugin-based architecture.
 .
 This package contains arch independent data for KDevelop.

Package: kdevelop-dev
Section: libdevel
Architecture: any
Depends: kdevelop55-libs (= ${binary:Version}),
         libgrantlee5-dev,
         libkf5archive-dev (>= 5.28.0),
         libkf5config-dev (>= 5.28.0),
         libkf5guiaddons-dev (>= 5.28.0),
         libkf5i18n-dev (>= 5.28.0),
         libkf5iconthemes-dev (>= 5.28.0),
         libkf5itemmodels-dev (>= 5.28.0),
         libkf5itemviews-dev (>= 5.28.0),
         libkf5jobwidgets-dev (>= 5.28.0),
         libkf5kcmutils-dev (>= 5.28.0),
         libkf5kio-dev (>= 5.28.0),
         libkf5newstuff-dev (>= 5.28.0),
         libkf5notifications-dev (>= 5.28.0),
         libkf5notifyconfig-dev (>= 5.28.0),
         libkf5parts-dev (>= 5.28.0),
         libkf5service-dev (>= 5.28.0),
         libkf5texteditor-dev (>= 5.28.0),
         libkf5threadweaver-dev (>= 5.28.0),
         libkf5windowsystem-dev (>= 5.28.0),
         qtbase5-dev (>= 5.7.0),
         qtdeclarative5-dev (>= 5.7.0),
         ${misc:Depends}
Breaks: kdevplatform-dev (<< 5.2)
Replaces: kdevplatform-dev (<< 5.2)
Description: development files for the KDevelop IDE
 KDevelop is a Free and Open Source integrated development
 environment (IDE). It provides editing, navigation and debugging features for
 several programming languages, as well as integration with multiple build
 systems and version-control systems, using a plugin-based architecture.
 .
 This package contains development files for KDevelop.

Package: kdevelop-l10n
Section: localization
Architecture: all
Depends: ${misc:Depends}
Replaces: kdevplatform-l10n (<< 5.2)
Breaks: kdevplatform-l10n (<< 5.2)
Description: localization files for the KDevelop IDE
 KDevelop is a Free and Open Source integrated development
 environment (IDE). It provides editing, navigation and debugging features for
 several programming languages, as well as integration with multiple build
 systems and version-control systems, using a plugin-based architecture.
 .
 This package contains the translations for KDevelop.

Package: plasma-kdevelop
Architecture: linux-any
Depends: kdevelop (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: KDevelop plugins for Plasma
 KDevelop is a Free and Open Source integrated development
 environment (IDE). It provides editing, navigation and debugging features for
 several programming languages, as well as integration with multiple build
 systems and version-control systems, using a plugin-based architecture.
 .
 This package contains the KDevelop plugins for integrating in the Plasma
 desktop environment:
  * sessions data engine
  * sessions widget
  * sessions KRunner module

Package: kdevplatform-dev
Section: oldlibs
Architecture: all
Depends: kdevelop-dev (>= ${source:Version}), ${misc:Depends}
Description: transitional package for kdevelop-dev
 This package is a transitional package, to upgrade to kdevelop-dev.
 It can be safely removed after the upgrade.
 .
 This package is part of the KDevelop platform.

Package: kdevplatform-l10n
Section: oldlibs
Architecture: all
Depends: kdevelop-l10n (>= ${source:Version}), ${misc:Depends}
Description: transitional package for kdevelop-l10n
 This package is a transitional package, to upgrade to kdevelop-l10n.
 It can be safely removed after the upgrade.
 .
 This package is part of the KDevelop platform.
